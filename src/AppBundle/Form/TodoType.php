<?php


namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;



class TodoType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $priority = array(
            'choices' => array(
                'niski'=>'niski',
                'normalny'=>'normalny',
                'wysoki'=>'wysoki'
            )

         );

        $builder
            ->add('name',TextType::class)
            ->add('description',TextareaType::class)
            ->add('priority', ChoiceType::class, $priority)
            ->add('date', DateTimeType::class);
    }

}