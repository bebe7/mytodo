<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;



class LocaleController extends Controller
{
    /**
     * @Route("/locate/{_locale}", name="locale")
     */
    public function localeAction(Request $request)
    {
        $newLocale = $request->getLocale();
        $this->get('session')->set('_locale', $newLocale);

        $previousUrl = $request->headers->get('referer');

        return $this->redirect($previousUrl);

    }

}

