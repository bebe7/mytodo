<?php


namespace AppBundle\Controller;

use AppBundle\Form\TodoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Todo;

use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\ArrayLoader;


class myTodoController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {

        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/all/{order}", name="all")
     */
    public function allAction($order = 'id')
    {

        if($order == 'id') {
            $todos = $this->getDoctrine()
                ->getRepository('AppBundle:Todo')
                ->findAllOrderedById();
        }
        else if($order == 'asc') {
            $todos = $this->getDoctrine()
                ->getRepository('AppBundle:Todo')
                ->findAllOrderedByName('ASC');
        } else {
            $todos = $this->getDoctrine()
                ->getRepository('AppBundle:Todo')
                ->findAllOrderedByName('DESC');
        }

        return $this->render('default/all.html.twig', array('todos'=>$todos, 'order'=>$order));

    }


    /**
     * @Route("/details/{id}", name="details")
     */
    public function detailisAction($id)
    {

        $todo = $this->getDoctrine()
            ->getRepository('AppBundle:Todo')
            ->find($id);

        $todosId = $this->getDoctrine()
            ->getRepository('AppBundle:Todo')
            ->findOnlyId();

        return $this->render('default/details.html.twig', array('todo'=>$todo, 'todosId'=>$todosId));

    }

    /**
     * @Route("/add", name="add")
     */
    public function addAction(Request $request)
    {
        $todo = new Todo();
        $form = $this->createForm(TodoType::class, $todo);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            // to DB
            $em = $this->getDoctrine()->getManager();
            $em->persist($todo);
            $em->flush();

            return $this->redirectToRoute('details', array('id'=>$todo->getId()));
        }

        return $this->render('default/add.html.twig', array('form'=>$form->createView()));
    }


    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function editAction(Request $request, $id)
    {

        $todo = $this->getDoctrine()
            ->getRepository('AppBundle:Todo')
            ->find($id);

        $form = $this->createForm(TodoType::class, $todo);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            // to DB
            $em = $this->getDoctrine()->getManager();
            $em->persist($todo);
            $em->flush();

            return $this->redirectToRoute('details', array('id'=>$todo->getId()));
        }

        return $this->render('default/edit.html.twig', array('todo'=>$todo, 'form'=>$form->createView()));
    }


    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $todo = $em->getRepository('AppBundle:Todo')->find($id);

        $em->remove($todo);
        $em->flush();

        return $this->redirectToRoute('all');
    }
}