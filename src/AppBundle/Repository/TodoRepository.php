<?php

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;



class TodoRepository extends EntityRepository
{

    public function findAllOrderedById()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT t FROM AppBundle:Todo t ORDER BY t.id DESC'
            )
            ->getResult();
    }

    public function findAllOrderedByName($whatOrder)
    {
        if($whatOrder == 'ASC') {
            $query = 'SELECT t FROM AppBundle:Todo t ORDER BY t.name ASC';
        } else {
            $query = 'SELECT t FROM AppBundle:Todo t ORDER BY t.name DESC';
        }

        return $this->getEntityManager()
            ->createQuery($query)
            ->getResult();
    }

    public function findAllOrderedByPriority()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT t FROM AppBundle:Todo t ORDER BY t.priority ASC'
            )
            ->getResult();
    }

    public function findOnlyId()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT t.id FROM AppBundle:Todo t ORDER BY t.id DESC'
            )
            ->getResult();
    }
}
